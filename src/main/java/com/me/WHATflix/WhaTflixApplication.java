package com.me.WHATflix;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class WhaTflixApplication {

	public static void main(String[] args) {
		SpringApplication.run(WhaTflixApplication.class, args);
	}
}
