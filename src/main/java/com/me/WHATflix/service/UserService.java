package com.me.WHATflix.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.me.WHATflix.exception.ResourceNotFoundException;
import com.me.WHATflix.model.User;
import com.me.WHATflix.repository.UserRepository;

@Service
public class UserService implements IUserService {

    @Autowired
    UserRepository userRepository;

    @Override
    public User getByUserId(int userId) {
        User user = userRepository.findOne(userId);
        if (user == null)
            throw new ResourceNotFoundException("USER", String.valueOf(userId));
        return user;
    }

    @Override
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }
}
