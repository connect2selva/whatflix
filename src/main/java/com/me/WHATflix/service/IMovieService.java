package com.me.WHATflix.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.me.WHATflix.resources.UserMovie;

@Service
public interface IMovieService {
    public List<String> findMoviesByUserId(int userId, List<String> searchText) throws Exception;

    public List<UserMovie> findMoviesForUsers();
}
