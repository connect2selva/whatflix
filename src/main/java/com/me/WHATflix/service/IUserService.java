package com.me.WHATflix.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.me.WHATflix.model.User;

@Service
public interface IUserService {

    public User getByUserId(int userId);

    public List<User> getAllUsers();
}
