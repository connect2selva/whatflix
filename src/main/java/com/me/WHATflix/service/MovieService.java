package com.me.WHATflix.service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.me.WHATflix.model.User;
import com.me.WHATflix.repository.MovieRepository;
import com.me.WHATflix.repository.filter.MovieFilter;
import com.me.WHATflix.resources.UserMovie;

@Service
public class MovieService implements IMovieService {

    private static Logger logger = LoggerFactory.getLogger(MovieService.class);

    @Autowired
    MovieRepository movieRepository;

    @Autowired
    IUserService userService;

    @Autowired
    CacheManager cacheManager;

    @PostConstruct
    public void init() {
        cacheManager.getCache("favoriteMovies").clear();
    }

    public List<String> findMoviesByUserId(int userId, List<String> searchText) throws Exception {
        List<String> results = new ArrayList<>();
        // 1. Get User Preferences
        // 2. Search by user preferences and search text from elastic search
        // 3. If results are empty from #2, then find movies by Search text

        // #1
        User user = userService.getByUserId(userId);
        if (user == null)
            return results;

        // #2
        MovieFilter.Builder fullRequest = MovieFilter.builder();
        fullRequest.withSearchText(searchText).withCast(user.getUserPreferences().getFavoriteActors())
                .withCrew("director", user.getUserPreferences().getFavoriteDirectors())
                .withLanguage(user.getUserPreferences().getPreferredLanguages());
        results = movieRepository.search(fullRequest.build());

        // #3
        if (results.isEmpty() && searchText != null) {
            MovieFilter.Builder request = MovieFilter.builder().withSearchText(searchText);
            results = movieRepository.search(request.build());
        }

        return results;
    }

    @Cacheable(cacheNames = "favoriteMovies")
    public List<UserMovie> findMoviesForUsers() {
        List<UserMovie> favoriteMoviesForAllUsers = new ArrayList<>();
        List<User> users = userService.getAllUsers();
        if (users != null) {
            users.forEach(user -> {
                try {
                    List<String> movies = findMoviesByUserId(user.getId(), null);
                    UserMovie userMovie = new UserMovie(user.getId(), movies);
                    favoriteMoviesForAllUsers.add(userMovie);
                } catch (Exception e) {
                    logger.error("Error while fetching favoriteMoviesForAllUsers", e);
                }
            });
        }
        return favoriteMoviesForAllUsers;
    }
}
