package com.me.WHATflix.exception;

import java.text.MessageFormat;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class ResourceNotFoundException extends RuntimeException {
    private static final long serialVersionUID = -3027910277081637025L;

    private final String resourceName;

    private final String resource;

    public ResourceNotFoundException(String resourceName, String resource) {
        super(MessageFormat.format("{0} is not found for resource {1}", resourceName, resource));
        this.resource = resource;
        this.resourceName = resourceName;
    }

    public String getResourceName() {
        return resourceName;
    }

    public String getResource() {
        return resource;
    }
}
