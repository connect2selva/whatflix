package com.me.WHATflix.config;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.annotation.PostConstruct;

import org.apache.http.HttpHost;
import org.elasticsearch.action.admin.indices.create.CreateIndexRequest;
import org.elasticsearch.action.admin.indices.get.GetIndexRequest;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.Request;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.Response;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class DataInitializer {

    private static final Logger logger = LoggerFactory.getLogger(DataInitializer.class);

    private static final String ES_MAPPING_FILE = "elasticsearch/movie_mapping.json";
    private static final String ES_TEST_DATA_FILE = "elasticsearch/movies.json";

    @Autowired
    ESConfig esConfig;

    @PostConstruct
    public void init() {
        try {
            initMoviesData();
        } catch (Exception e) {
            logger.error("Data Initiailization failed", e);
            System.exit(1);
        }
    }

    private void initMoviesData() throws Exception {

        String mapping = readMapping();

        Thread.sleep(3 * 1000); // Just to give time to up elastic search server

        RestHighLevelClient client = new RestHighLevelClient(
                RestClient.builder(new HttpHost(esConfig.getHost(), esConfig.getPort(), "http")));

        GetIndexRequest getIndexRequest = new GetIndexRequest();
        getIndexRequest.indices(esConfig.getIndex());

        Response response = client.getLowLevelClient().performRequest(new Request("HEAD", "/" + esConfig.getIndex()));
        boolean isExists = response.getStatusLine().getStatusCode() == 200;
        if (!isExists) {
            // Create Index
            CreateIndexRequest createIndexRequest = new CreateIndexRequest(esConfig.getIndex());
            createIndexRequest.mapping(esConfig.getType(), mapping, XContentType.JSON);
            client.indices().create(createIndexRequest, RequestOptions.DEFAULT);

            // Index data
            BulkRequest request = new BulkRequest();
            JsonNode node = readMoviesData();
            node.forEach(n -> request.add(
                    new IndexRequest(esConfig.getIndex(), esConfig.getType()).source(n.toString(), XContentType.JSON)));
            BulkResponse bulkResponse = client.bulk(request, RequestOptions.DEFAULT);
            if (bulkResponse.hasFailures()) {
                bulkResponse.forEach(f -> logger.error(f.getFailureMessage()));
            }

            client.getLowLevelClient().performRequest(new Request("POST", "/" + esConfig.getIndex() + "/_refresh"));
        }
        client.close();
    }

    private String readMapping() throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();
        try (InputStream inputStream = classLoader.getResourceAsStream(ES_MAPPING_FILE)) {
            return readFromInputStream(inputStream);
        }
    }

    private JsonNode readMoviesData() throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();
        ObjectMapper mapper = new ObjectMapper();
        try (InputStream inputStream = classLoader.getResourceAsStream(ES_TEST_DATA_FILE)) {
            return mapper.readTree(inputStream);
        }
    }

    private String readFromInputStream(InputStream inputStream) throws IOException {
        StringBuilder resultStringBuilder = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(inputStream))) {
            String line;
            while ((line = br.readLine()) != null) {
                resultStringBuilder.append(line).append("\n");
            }
        }
        return resultStringBuilder.toString();
    }
}
