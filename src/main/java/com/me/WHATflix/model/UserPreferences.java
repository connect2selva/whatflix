package com.me.WHATflix.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import com.vladmihalcea.hibernate.type.array.StringArrayType;

@TypeDefs({ @TypeDef(name = "string-array", typeClass = StringArrayType.class) })
@Entity
@Table(name = "user_preferences")
@NamedQuery(name = "UserPreferences.findAll", query = "SELECT u FROM UserPreferences u")
public class UserPreferences {
    @Id
    public int id;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Type(type = "string-array")
    @Column(name = "favourite_actors")
    private String[] favoriteActors;

    @Type(type = "string-array")
    @Column(name = "favourite_directors")
    private String[] favoriteDirectors;

    @Type(type = "string-array")
    @Column(name = "preferred_languages")
    private String[] preferredLanguages;

    public int getId() {
        return id;
    }

    public String[] getFavoriteActors() {
        return favoriteActors;
    }

    public String[] getFavoriteDirectors() {
        return favoriteDirectors;
    }

    public String[] getPreferredLanguages() {
        return preferredLanguages;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setFavoriteActors(String[] favoriteActors) {
        this.favoriteActors = favoriteActors;
    }

    public void setFavoriteDirectors(String[] favoriteDirectors) {
        this.favoriteDirectors = favoriteDirectors;
    }

    public void setPreferredLanguages(String[] preferredLanguages) {
        this.preferredLanguages = preferredLanguages;
    }
}
