package com.me.WHATflix.repository;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.annotation.PostConstruct;

import org.apache.http.HttpHost;
import org.elasticsearch.action.ActionListener;
import org.elasticsearch.action.search.ClearScrollRequest;
import org.elasticsearch.action.search.ClearScrollResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchScrollRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.FieldSortBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.me.WHATflix.config.ESConfig;
import com.me.WHATflix.repository.filter.MovieFilter;

@Repository
public class ESMovieRepository implements MovieRepository {

    @Autowired
    ESConfig esConfig;

    private RestHighLevelClient client;

    private static Logger logger = LoggerFactory.getLogger(ESMovieRepository.class);

    @PostConstruct
    private void init() throws UnknownHostException {
        client = new RestHighLevelClient(
                RestClient.builder(new HttpHost(esConfig.getHost(), esConfig.getPort(), "http")));
    }

    @Override
    public List<String> search(MovieFilter request) throws Exception {
        if (request == null)
            return Collections.emptyList();

        return executeSearchQuery(request);
    }

    private List<String> executeSearchQuery(MovieFilter request)
            throws InterruptedException, ExecutionException, IOException {

        QueryBuilder qb = request.getESQuery();

        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder().query(qb).fetchSource(true)
                .fetchSource("original_title", "_type")
                .sort(new FieldSortBuilder("original_title").order(SortOrder.ASC)).from(request.getFrom())
                .size(request.getSize());

        SearchRequest searchRequest = new SearchRequest(esConfig.getIndex());
        searchRequest.types(esConfig.getType());
        searchRequest.source(searchSourceBuilder);
        searchRequest.scroll(TimeValue.timeValueMinutes(1L));

        SearchResponse response = client.search(searchRequest, RequestOptions.DEFAULT);

        if (request.isFetchAll()) {
            return getAllResults(response);

        } else {
            return getResults(response);
        }
    }

    private List<String> getAllResults(SearchResponse response) throws IOException {
        String scrollId = response.getScrollId();

        List<String> movies = new ArrayList<>();
        do {
            movies.addAll(getResults(response));
            SearchScrollRequest scrollRequest = new SearchScrollRequest(scrollId);
            scrollRequest.scroll(TimeValue.timeValueSeconds(30));
            response = client.scroll(scrollRequest, RequestOptions.DEFAULT);
            scrollId = response.getScrollId();
        } while (response.getHits().getHits().length != 0);

        clearScroll(scrollId);
        return movies;
    }

    private void clearScroll(String scrollId) {
        ClearScrollRequest request = new ClearScrollRequest();
        request.addScrollId(scrollId);
        client.clearScrollAsync(request, RequestOptions.DEFAULT, new ActionListener<ClearScrollResponse>() {
            @Override
            public void onResponse(ClearScrollResponse clearScrollResponse) {
                logger.debug("Scoll cleared");
            }

            @Override
            public void onFailure(Exception e) {
                logger.error("Scoll clear failed");
            }
        });
    }

    private List<String> getResults(SearchResponse response) {
        List<String> movies = new ArrayList<>();
        for (SearchHit hit : response.getHits().getHits()) {
            String movie = hit.getSourceAsMap().get("original_title").toString();
            movies.add(movie);
        }
        return movies;
    }
}
