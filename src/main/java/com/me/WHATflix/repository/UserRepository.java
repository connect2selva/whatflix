package com.me.WHATflix.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.me.WHATflix.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
}
