package com.me.WHATflix.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.me.WHATflix.repository.filter.MovieFilter;

@Repository
public interface MovieRepository {

	public List<String> search(MovieFilter request) throws Exception;
}
