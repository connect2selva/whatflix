package com.me.WHATflix.repository.filter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.apache.lucene.search.join.ScoreMode;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;

public class MovieFilter {
    private static final int DEFAULT_SIZE = 50;

    private List<String> cast;
    private Map<String, List<String>> crew;
    private List<String> languages;
    private List<String> searchText;
    private int from;
    private boolean fetchAll;

    private int size = DEFAULT_SIZE;

    public MovieFilter(Builder builder) {
        setCast(builder.cast);
        setCrew(builder.crew);
        setLanguages(builder.languages);
        setSearchText(builder.searchText);
        setFetchAll(true);

    }

    public List<String> getCast() {
        return cast;
    }

    public Map<String, List<String>> getCrew() {
        return crew;
    }

    public List<String> getLanguages() {
        return languages;
    }

    public void setCast(List<String> cast) {
        this.cast = cast;
    }

    public void setCrew(Map<String, List<String>> crew) {
        this.crew = crew;
    }

    public void setLanguages(List<String> languages) {
        this.languages = languages;
    }

    public List<String> getSearchText() {
        return searchText;
    }

    public void setSearchText(List<String> searchText) {
        this.searchText = searchText;
    }

    public int getFrom() {
        return from;
    }

    public int getSize() {
        return size;
    }

    public void setFrom(int from) {
        this.from = from;
    }

    public void setSize(int size) {
        setFetchAll(false);
        this.size = size;
    }

    public boolean isFetchAll() {
        return fetchAll;
    }

    public void setFetchAll(boolean fetchAll) {
        this.fetchAll = fetchAll;
    }

    public QueryBuilder getESQuery() {
        BoolQueryBuilder bqb = QueryBuilders.boolQuery();
        bqb.must(getSearchTextQuery());
        for (QueryBuilder qb : getOtherQuery()) {
            bqb.must(qb);
        }

        return bqb;
    }

    private QueryBuilder getSearchTextQuery() {
        List<QueryBuilder> searchTextQueryBuilders = new ArrayList<>();
        BoolQueryBuilder searchTextBqb = QueryBuilders.boolQuery();
        if (getSearchText() != null && !getSearchText().isEmpty()) {
            String query = searchText.stream().map(s -> "\"" + s + "\"").collect(Collectors.joining(" OR "));
            searchTextQueryBuilders.add(QueryBuilders.queryStringQuery("original_title_ft:" + query));
            searchTextQueryBuilders.add(QueryBuilders.nestedQuery("cast",
                    QueryBuilders.queryStringQuery("cast.name_ft:" + query), ScoreMode.Avg));
            searchTextQueryBuilders.add(QueryBuilders.nestedQuery("crew",
                    QueryBuilders.queryStringQuery("crew.name_ft: (" + query + ") AND crew.job:Director"),
                    ScoreMode.Avg));
        }
        for (QueryBuilder qb : searchTextQueryBuilders) {
            searchTextBqb.should(qb);
        }
        return searchTextBqb;
    }

    private List<QueryBuilder> getOtherQuery() {
        List<QueryBuilder> qb = new ArrayList<>();
        if (this.getCast() != null && !this.getCast().isEmpty()) {
            qb.add((QueryBuilders.nestedQuery("cast", QueryBuilders.termsQuery("cast.name", this.getCast()),
                    ScoreMode.Avg)));
        }
        if (this.getCrew() != null && !this.getCrew().isEmpty()) {
            for (Entry<String, List<String>> e : this.getCrew().entrySet()) {
                qb.add(QueryBuilders.nestedQuery("crew",
                        QueryBuilders.boolQuery().must(QueryBuilders.termsQuery("crew.name", e.getValue()))
                                .must(QueryBuilders.matchQuery("crew.job", e.getKey())),
                        ScoreMode.Avg));
            }
        }
        if (this.getLanguages() != null && !this.getLanguages().isEmpty()) {
            qb.add(QueryBuilders.termsQuery("spoken_languages", this.getLanguages()));
        }
        return qb;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private List<String> cast;
        private Map<String, List<String>> crew;
        private List<String> languages;
        private List<String> searchText;

        public Builder withSearchText(List<String> searchText) {
            this.searchText = searchText;
            return this;
        }

        public Builder withCrew(Map<String, List<String>> crew) {
            this.crew = crew;
            return this;
        }

        public Builder withCrew(String job, String[] values) {
            if (this.crew == null)
                this.crew = new HashMap<>();
            this.crew.put(job, Arrays.asList(values));
            return this;
        }

        public Builder withLanguage(String[] languages) {
            this.languages = Arrays.asList(languages);
            return this;
        }

        public Builder withCast(String[] cast) {
            this.cast = Arrays.asList(cast);
            return this;
        }

        public MovieFilter build() {
            return new MovieFilter(this);
        }
    }

    @Override
    public int hashCode() {
        return this.getESQuery().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof MovieFilter))
            return false;

        MovieFilter that = (MovieFilter) obj;
        return this.getESQuery().equals(that.getESQuery());
    }
}
