package com.me.WHATflix.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.me.WHATflix.resources.UserMovie;
import com.me.WHATflix.service.IMovieService;

import io.swagger.annotations.Api;

@Api(value = "Movies")
@RestController
@RequestMapping(value = "/movies")
public class MovieController {

    @Autowired
    IMovieService movieService;

    @RequestMapping(value = "/user/{userId}/search", method = RequestMethod.GET, produces = "application/json")
    public List<String> findMoviesByUserId(@PathVariable("userId") int userId,
            @RequestParam(value = "text", required = false) List<String> searchText) throws Exception {
        return movieService.findMoviesByUserId(userId, searchText);

    }

    @RequestMapping(value = "/users", method = RequestMethod.GET, produces = "application/json")
    public List<UserMovie> findMoviesForUsers() {
        return movieService.findMoviesForUsers();

    }
}
