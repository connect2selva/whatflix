package com.me.WHATflix.resources;

import java.io.Serializable;
import java.util.List;

public class UserMovie implements Serializable {
    private static final long serialVersionUID = 1372736897956450253L;
    private int user;
    private List<String> movies;

    public UserMovie(int user, List<String> movies) {
        setUser(user);
        setMovies(movies);
    }
    
    public UserMovie(){
        
    }

    public int getUser() {
        return user;
    }

    public List<String> getMovies() {
        return movies;
    }

    public void setUser(int user) {
        this.user = user;
    }

    public void setMovies(List<String> movies) {
        this.movies = movies;
    }
}
