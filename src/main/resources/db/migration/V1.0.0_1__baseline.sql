CREATE TABLE users
(
    id int not null primary key,
	first_name VARCHAR(100),
	last_name VARCHAR(100)
);

CREATE TABLE user_preferences
(
    id serial primary key,
    user_id int not null,
    preferred_languages character varying(50)[],
    favourite_actors  character varying(100)[],
    favourite_directors character varying(100)[],
    
    CONSTRAINT fk_user_preferences_user_id FOREIGN KEY(user_id)
		REFERENCES users(id) MATCH SIMPLE
		ON UPDATE NO ACTION
        ON DELETE CASCADE
);

INSERT INTO users values (100,'James','Anderson');
INSERT INTO user_preferences (user_id,preferred_languages,favourite_actors,favourite_directors) values (100,ARRAY ['English','Spanish'],ARRAY['Denzel Washington','Kate Winslet','Emma Suárez','Tom Hanks'],ARRAY['Steven Spielberg','Martin Scorsese','Pedro Almodóvar']);

INSERT INTO users values (101,'Kevin','Pieterson');
insert into user_preferences(user_id,preferred_languages,favourite_actors,favourite_directors) values ('101',ARRAY['English'],ARRAY ['Denzel Washington','Anne Hathaway','Tom Hanks'],ARRAY['Guy Ritchie','Quentin Tarantino']);

INSERT INTO users values (102,'Andrew','Flintoff');
insert into user_preferences(user_id,preferred_languages,favourite_actors,favourite_directors) values ('102',ARRAY['English'],ARRAY ['Uma Thurman','Charlize Theron','John Travolta'],ARRAY['Quentin Tarantino']);

INSERT INTO users values (103,'Glenn','Mcgrath');
insert into user_preferences(user_id,preferred_languages,favourite_actors,favourite_directors) values ('103',ARRAY['English'],ARRAY['Antonio Banderas','Clint Eastwood','Emma Suárez','Bruce Willis'],ARRAY['Stanley Kubrick','Oliver Stone']);

INSERT INTO users values (104,'Shane','Warne');
insert into user_preferences(user_id,preferred_languages,favourite_actors,favourite_directors) values ('104',ARRAY['English'],ARRAY['Anthony Hopkins','Adam Sandler','Bruce Willis'],ARRAY['Nora Ephron','Oliver Stone']);

INSERT INTO users values (105,'Ricky','Ponting');
insert into user_preferences(user_id,preferred_languages,favourite_actors,favourite_directors) values ('105',ARRAY['Spanish'],ARRAY['Anthony Hopkins','Bárbara Goenaga','Tenoch Huerta'],ARRAY['Amat Escalante','Robert Rodriguez']);

INSERT INTO users values (106,'James','Maxwell');
insert into user_preferences(user_id,preferred_languages,favourite_actors,favourite_directors) values ('106',ARRAY['English','Spanish'],ARRAY['Brad Pitt','Robert Downey Jr.','Jennifer Lawrence','Johnny Depp'],ARRAY['Steven Spielberg','Martin Scorsese','Ridley Scott']);
 
 