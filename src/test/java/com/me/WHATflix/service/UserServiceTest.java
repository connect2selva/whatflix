package com.me.WHATflix.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import com.me.WHATflix.exception.ResourceNotFoundException;
import com.me.WHATflix.model.User;
import com.me.WHATflix.repository.UserRepository;

@RunWith(SpringRunner.class)
public class UserServiceTest {

    @TestConfiguration
    static class TestContextConfiguration {
        @Bean
        public IUserService userService() {
            return new UserService();
        }
    }

    @MockBean
    UserRepository userRepoMock;

    @Autowired
    private IUserService userService;

    @Test
    public void testGetByUserId() {
        // given
        User user = new User();
        user.setId(100);
        given(userRepoMock.findOne(100)).willReturn(user);

        // when
        User result = userService.getByUserId(100);

        // then
        assertEquals(100, result.getId());
    }

    @Test
    public void testGetByUserId_NotFound() {
        // given
        given(userRepoMock.findOne(100)).willReturn(null);

        // when
        try {
            userService.getByUserId(100);
        } catch (ResourceNotFoundException e) {
            assertEquals("USER", e.getResourceName());
            assertEquals("100", e.getResource());
        }

    }

    @Test
    public void testGetAllUsers() {
        // given
        given(userRepoMock.findAll()).willReturn(Arrays.asList(new User[] { new User() }));

        // when
        List<User> users = userService.getAllUsers();

        // then
        assertEquals(1, users.size());
    }
}
