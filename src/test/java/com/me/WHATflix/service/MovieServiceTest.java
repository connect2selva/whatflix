package com.me.WHATflix.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.cache.CacheManager;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import com.me.WHATflix.model.User;
import com.me.WHATflix.model.UserPreferences;
import com.me.WHATflix.repository.MovieRepository;
import com.me.WHATflix.repository.filter.MovieFilter;
import com.me.WHATflix.resources.UserMovie;

@RunWith(SpringRunner.class)
public class MovieServiceTest {

    @TestConfiguration
    static class TestContextConfiguration {
        @Bean
        public IMovieService movieService() {
            return new MovieService();
        }
        
        @Bean
        public CacheManager cacheManager() {
            return new ConcurrentMapCacheManager();
        }
    }

    String[] actors;
    String[] dir;
    String[] lang;
    User mockUser;
    List<String> searchText;
    Map<String, List<String>> crew;

    @MockBean
    MovieRepository movieRepoMock;

    @MockBean
    UserService userServiceMock;

    @Autowired
    private IMovieService movieService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        this.actors = new String[] { "Tom Hanks" };
        this.dir = new String[] { "Steven Spielberg" };
        this.lang = new String[] { "English" };
        this.mockUser = Mockito.mock(User.class);
        this.searchText = Arrays.asList("Tom Hanks");
        crew = new HashMap<>();
        crew.put("director", Arrays.asList(dir));

        given(userServiceMock.getByUserId(100)).willReturn(mockUser);
        given(mockUser.getId()).willReturn(100);
        given(mockUser.getUserPreferences()).willReturn(Mockito.mock(UserPreferences.class));
        given(mockUser.getUserPreferences().getFavoriteActors()).willReturn(actors);
        given(mockUser.getUserPreferences().getFavoriteDirectors()).willReturn(dir);
        given(mockUser.getUserPreferences().getPreferredLanguages()).willReturn(lang);
    }

    @Test
    public void testfindMoviesByUserId() throws Exception {
        // given
        MovieFilter request = MovieFilter.builder().withSearchText(searchText).withCast(actors)
                .withCrew(crew).withLanguage(lang).build();

        given(movieRepoMock.search(request)).willReturn(Arrays.asList("Movie1"));

        // when
        List<String> list = movieService.findMoviesByUserId(100, searchText);

        // then
        assertEquals("Movie1", list.get(0));
    }

    @Test
    public void testfindMoviesByUserId_InvalidUser() throws Exception {
        // given
        List<String> searchText = Arrays.asList("Tom Hanks");
        given(userServiceMock.getByUserId(100)).willReturn(null);

        // when
        List<String> list = movieService.findMoviesByUserId(100, searchText);

        // then
        assertTrue(list.isEmpty());
    }

    @Test
    public void testfindMoviesByUserId_EmptyResultsForUserPreferences() throws Exception {
        // given

        MovieFilter.Builder fullRequest = MovieFilter.builder().withSearchText(searchText)
                .withCast(actors).withCrew(crew).withLanguage(lang);

        MovieFilter.Builder request = MovieFilter.builder().withSearchText(searchText);

        given(movieRepoMock.search(fullRequest.build())).willReturn(Collections.emptyList());
        given(movieRepoMock.search(request.build())).willReturn(Arrays.asList("Movie1"));

        // when
        List<String> list = movieService.findMoviesByUserId(100, searchText);

        // then
        assertEquals("Movie1", list.get(0));
        then(movieRepoMock).should().search(request.build());
    }

    @Test
    public void testfindMoviesByUserId_EmptySearchText() throws Exception {
        // given
        List<String> searchText = Collections.emptyList();
        Map<String, List<String>> crew = new HashMap<>();
        crew.put("director", Arrays.asList(dir));
        MovieFilter.Builder fullRequest = MovieFilter.builder().withSearchText(searchText)
                .withCast(actors).withCrew(crew).withLanguage(lang);

        given(movieRepoMock.search(fullRequest.build())).willReturn(Arrays.asList("Movie1"));

        // when
        List<String> list = movieService.findMoviesByUserId(100, searchText);

        // then
        assertEquals("Movie1", list.get(0));
    }

    @Test
    public void testFindMoviesForUsers() throws Exception {
        // given
        List<String> searchText = Collections.emptyList();
        MovieFilter request = MovieFilter.builder().withSearchText(searchText).withCast(actors)
                .withCrew(crew).withLanguage(lang).build();
        given(userServiceMock.getAllUsers()).willReturn(Arrays.asList(new User[] { mockUser }));
        given(movieRepoMock.search(request)).willReturn(Arrays.asList("Movie1"));

        // when
        List<UserMovie> moviesbyUsers = movieService.findMoviesForUsers();

        // then
        assertEquals("Movie1", moviesbyUsers.get(0).getMovies().get(0));
        assertEquals(mockUser.getId(), moviesbyUsers.get(0).getUser());
    }

    @Test
    public void testFindMoviesForUsers_EmptyUsers() throws Exception {
        // given
        List<String> searchText = Collections.emptyList();
        MovieFilter request = MovieFilter.builder().withSearchText(searchText).withCast(actors)
                .withCrew(crew).withLanguage(lang).build();
        given(userServiceMock.getAllUsers()).willReturn(null);
        given(movieRepoMock.search(request)).willReturn(Arrays.asList("Movie1"));

        // when
        List<UserMovie> moviesbyUsers = movieService.findMoviesForUsers();

        // then
        assertEquals(0, moviesbyUsers.size());
    }

    @Test
    public void testFindMoviesForUsers_Exception() throws Exception {
        // given
        List<String> searchText = Collections.emptyList();
        MovieFilter request = MovieFilter.builder().withSearchText(searchText).withCast(actors)
                .withCrew(crew).withLanguage(lang).build();
        given(userServiceMock.getAllUsers()).willReturn(Arrays.asList(new User[] { mockUser }));
        given(movieRepoMock.search(request)).willThrow(new Exception("Ignore, Junit Testing"));

        // when
        List<UserMovie> moviesbyUsers = movieService.findMoviesForUsers();

        // then
        assertEquals(0, moviesbyUsers.size());
    }
}
