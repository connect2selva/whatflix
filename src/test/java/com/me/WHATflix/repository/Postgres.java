package com.me.WHATflix.repository;

import org.flywaydb.core.Flyway;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.support.TestPropertySourceUtils;
import org.testcontainers.containers.PostgreSQLContainer;
public class Postgres {

    @SuppressWarnings("rawtypes")
    public static PostgreSQLContainer postgreSQLContainer = (PostgreSQLContainer) new PostgreSQLContainer(
            "postgres:9.6");

    public static class Initializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
        public Initializer() {
        }
        public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
            postgreSQLContainer.start();
            TestPropertySourceUtils.addInlinedPropertiesToEnvironment(configurableApplicationContext,
                    "spring.datasource.url=" + postgreSQLContainer.getJdbcUrl(),
                    "spring.datasource.username=" + postgreSQLContainer.getUsername(),
                    "spring.datasource.password=" + postgreSQLContainer.getPassword());

            Flyway flyway = new Flyway();
            flyway.setDataSource(postgreSQLContainer.getJdbcUrl(), postgreSQLContainer.getUsername(),
                    postgreSQLContainer.getPassword());
            flyway.migrate();
        }

    }
    
    public static void stop(){
        postgreSQLContainer.stop();
    }
}
