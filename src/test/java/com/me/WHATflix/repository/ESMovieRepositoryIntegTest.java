package com.me.WHATflix.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.elasticsearch.action.admin.indices.get.GetIndexRequest;
import org.elasticsearch.client.Request;
import org.elasticsearch.client.Response;
import org.elasticsearch.client.RestHighLevelClient;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import com.me.WHATflix.config.DataInitializer;
import com.me.WHATflix.config.ESConfig;
import com.me.WHATflix.repository.filter.MovieFilter;

@RunWith(SpringRunner.class)
public class ESMovieRepositoryIntegTest {

    private static ESConfig esConfig = null;
    private static ElasticSearch elasticSearch = null;

    @TestConfiguration
    static class ESMovieRepositoryITTestContextConfiguration {
        @Bean
        public MovieRepository movieRepository() {
            return new ESMovieRepository();
        }

        @Bean
        public ESConfig esClient() {
            return ESMovieRepositoryIntegTest.esConfig;
        }

        @Bean
        public DataInitializer dataInitializer() {
            return new DataInitializer();
        }
    }

    private static RestHighLevelClient client;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private DataInitializer dataInitializer;

    @BeforeClass
    public static void init() throws Exception {

        esConfig = new ESConfig();
        esConfig.setIndex("movies_test");
        esConfig.setType("movies");

        elasticSearch = new ElasticSearch();
        client = elasticSearch.setup(esConfig);

    }

    @Before
    public void setup() throws Exception {
        GetIndexRequest getIndexRequest = new GetIndexRequest();
        getIndexRequest.indices(esConfig.getIndex());

        Response response = client.getLowLevelClient().performRequest(new Request("HEAD", "/" + esConfig.getIndex()));
        boolean isExists = response.getStatusLine().getStatusCode() == 200;
        if (isExists)
            return;

        // createIndex(esConfig);
        dataInitializer.init();

    }

    @Test
    public void testSearch_empty() throws Exception {
        List<String> result = movieRepository.search(null);
        assertEquals(0, result.size());

    }

    @Test
    public void testSearch_SearchText_title() throws Exception {
        MovieFilter request = MovieFilter.builder().withSearchText(Arrays.asList("Pirates", "Superman")).build();
        List<String> result = movieRepository.search(request);
        assertEquals(4, result.size());
        assertTrue(result.get(0).equalsIgnoreCase("Batman v Superman: Dawn of Justice"));
    }

    @Test
    public void testSearch_SearchText_Cast() throws Exception {
        MovieFilter request = MovieFilter.builder().withSearchText(Arrays.asList("Daniel Craig", "Emma Stone")).build();
        List<String> result = movieRepository.search(request);

        assertEquals(2, result.size());
        assertTrue(result.get(0).equalsIgnoreCase("Skyfall"));
    }

    @Test
    public void testSearch_SearchText_Director() throws Exception {
        MovieFilter request = MovieFilter.builder().withSearchText(Arrays.asList("Steven Spielberg")).build();
        List<String> result = movieRepository.search(request);

        assertEquals(1, result.size());
        assertTrue(result.get(0).equalsIgnoreCase("Indiana Jones and the Kingdom of the Crystal Skull"));
    }

    @Test
    public void testSearch_SearchText_NoResult() throws Exception {
        MovieFilter request = MovieFilter.builder().withSearchText(Arrays.asList("teven Spielberg")).build();
        List<String> result = movieRepository.search(request);

        assertEquals(0, result.size());
    }

    @Test
    public void testSearch_Preferences() throws Exception {
        Map<String, List<String>> crew = new HashMap<>();
        crew.put("director", Arrays.asList("Justin Lin", "Steven Spielberg", "Brett Ratner"));
        MovieFilter request = MovieFilter.builder()
                .withCast(new String[] { "Harrison Ford", "George Lucas", "Karl Urban", "Jackie Chan" }).withCrew(crew)
                .withLanguage(new String[] { "Deutsch", "English" }).build();
        request.setSize(2);
        request.setFetchAll(true);
        List<String> result = movieRepository.search(request);

        assertEquals(3, result.size());
        assertTrue(result.get(0).equalsIgnoreCase("Indiana Jones and the Kingdom of the Crystal Skull"));
    }

    @Test
    public void testSearch_Preferences_searchText() throws Exception {
        Map<String, List<String>> crew = new HashMap<>();
        crew.put("director", Arrays.asList("Justin Lin", "Steven Spielberg", "Brett Ratner"));
        MovieFilter request = MovieFilter.builder()
                .withCast(new String[] { "Harrison Ford", "George Lucas", "Karl Urban", "Jackie Chan" }).withCrew(crew)
                .withLanguage(new String[] { "Deutsch", "English" }).withSearchText(Arrays.asList("Rush")).build();
        List<String> result = movieRepository.search(request);

        assertEquals(1, result.size());
        assertTrue(result.get(0).equalsIgnoreCase("Rush Hour 3"));
    }

    @Test
    public void testSearch_Preferences_topScored() throws Exception {
        Map<String, List<String>> crew = new HashMap<>();
        crew.put("director", Arrays.asList("Justin Lin", "Steven Spielberg", "Brett Ratner", "Jon Favreau"));
        MovieFilter request = MovieFilter.builder()
                .withCast(new String[] { "Chris Pine", "Josh Hutcherson", "Jennifer Lawrence", "Max von Sydow",
                        "Hiroyuki Sanada", "Jackie Chan", "Robert Downey Jr", "Gwyneth Paltrow", "Harrison Ford" })
                .withCrew(crew).withLanguage(new String[] { "Deutsch", "English" }).build();
        request.setFrom(0);
        request.setSize(3);
        List<String> result = movieRepository.search(request);
        assertEquals(3, result.size());
        assertTrue(result.get(0).equalsIgnoreCase("Indiana Jones and the Kingdom of the Crystal Skull"));
    }

    @AfterClass
    public static void tearDown() throws IOException {
        if (elasticSearch != null)
            elasticSearch.stop();
    }

}
