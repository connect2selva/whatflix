package com.me.WHATflix.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.me.WHATflix.model.User;
import com.me.WHATflix.model.UserPreferences;
import com.me.WHATflix.repository.Postgres.Initializer;

@RunWith(SpringRunner.class)
@DataJpaTest
@ContextConfiguration(initializers = { Initializer.class })
@AutoConfigureTestDatabase(replace = NONE)

public class UserRepositoryIntegTest {

    @Autowired
    private UserRepository userRepository;

    private List<User> users = new ArrayList<>();

    @Before
    public void setUp() throws Exception {
        User user1 = new User();
        User user2 = new User();
        UserPreferences userPreferences1 = new UserPreferences();
        userPreferences1.setId(100);

        user1.setId(200);
        user1.setFirstName("Thomas");
        user1.setLastName("McGanor");
        user1.setUserPreferences(userPreferences1);
        userPreferences1.setFavoriteDirectors(new String[] { "dir1", "dir2", "dir3" });
        userPreferences1.setFavoriteActors(new String[] { "act1", "act2", "act3" });
        userPreferences1.setPreferredLanguages(new String[] { "English", "Hindi" });
        userPreferences1.setUser(user1);

        user2.setId(201);
        user2.setFirstName("Micheal");
        user2.setLastName("Vahaun");

        this.userRepository.save(user1);
        this.userRepository.save(user2);

        users.add(user1);
        users.add(user2);
    }

    @Test
    public void testFetchData() {
        /* Test data retrieval */
        User userA = userRepository.findOne(200);
        assertNotNull(userA);
        assertEquals(true, userA.getFirstName().equals("Thomas"));
        assertEquals(true, userA.getLastName().equals("McGanor"));
        assertNotNull(userA.getId());
        assertEquals(200, userA.getId());
        assertEquals(Arrays.asList("dir1", "dir2", "dir3"),
                Arrays.asList(userA.getUserPreferences().getFavoriteDirectors()));
        assertEquals(Arrays.asList("act1", "act2", "act3"),
                Arrays.asList(userA.getUserPreferences().getFavoriteActors()));
        assertEquals(Arrays.asList("English", "Hindi"),
                Arrays.asList(userA.getUserPreferences().getPreferredLanguages()));
        assertEquals(100, userA.getUserPreferences().getId());
        assertEquals(200, userA.getUserPreferences().getUser().getId());
    }

    @AfterClass
    public static void tearDown() {
        Postgres.stop();
    }
}
