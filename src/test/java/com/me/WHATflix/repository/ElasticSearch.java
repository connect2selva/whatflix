package com.me.WHATflix.repository;

import java.io.IOException;

import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.flywaydb.core.Flyway;
import org.json.JSONException;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.support.TestPropertySourceUtils;
import org.testcontainers.elasticsearch.ElasticsearchContainer;

import com.me.WHATflix.config.ESConfig;

public class ElasticSearch {

    private static ElasticsearchContainer container = null;
    private static RestHighLevelClient client = null;
    private static ESConfig esConfig = null;

    public static void stopRestClient() throws IOException {
        client.close();
    }

    public static void stopContainer() {
        container.stop();
    }

    public static RestHighLevelClient setup(ESConfig esConfig) throws Exception {
        container = new ElasticsearchContainer("docker.elastic.co/elasticsearch/elasticsearch:6.5.0");
        container.withEnv("ELASTIC_PASSWORD", "changeme");
        container.start();

        String host = container.getHttpHostAddress();
        esConfig.setHost(host.split(":")[0]);
        esConfig.setPort(Integer.parseInt(host.split(":")[1]));
        initClient(esConfig);
        return client;
    }

    public static void stop() throws IOException {
        stopRestClient();
        stopContainer();
    }

    private static void initClient(ESConfig esConfig) throws JSONException, IOException {
        final CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
        credentialsProvider.setCredentials(AuthScope.ANY, new UsernamePasswordCredentials("elastic", "changeme"));
        client = new RestHighLevelClient(
                RestClient.builder(HttpHost.create(esConfig.toHost())).setHttpClientConfigCallback(
                        httpClientBuilder -> httpClientBuilder.setDefaultCredentialsProvider(credentialsProvider)));
    }

    public static class Initializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
        public Initializer() {
        }

        public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
            esConfig = new ESConfig();
            esConfig.setIndex("movies_test");
            esConfig.setType("movies");

            try {
                client = ElasticSearch.setup(esConfig);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    public static ESConfig esConfig() {
        return esConfig;
    }
}
