package com.me.WHATflix.repository;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import javax.xml.bind.DatatypeConverter;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import com.me.WHATflix.repository.filter.MovieFilter;

@RunWith(SpringRunner.class)
public class MovieFilterTest {

    @Test
    public void test() {

    }
    
    @Test
    public void testESQuery_searchText() throws IOException, NoSuchAlgorithmException {
        String fileString = new String(
                Files.readAllBytes(Paths
                        .get(getClass().getClassLoader().getResource("elasticsearch/query/searchText.json").getFile())),
                StandardCharsets.UTF_8);

        MovieFilter request = MovieFilter.builder().withSearchText(Arrays.asList("foo", "bar")).build();

        assertTrue(checksum(fileString).equalsIgnoreCase(checksum(request.getESQuery().toString())));
    }

    @Test
    public void testESQuery_Prefrences_1() throws NoSuchAlgorithmException, IOException {

        String fileString = new String(
                Files.readAllBytes(Paths.get(
                        getClass().getClassLoader().getResource("elasticsearch/query/preferences.json").getFile())),
                StandardCharsets.UTF_8);

        MovieFilter request = MovieFilter.builder()
                .withCast(new String[]{"bar1", "bar2", "bar3", "bar4"}).withCrew("director",new String[]{"foo1", "foo2", "foo3"})
                .withLanguage(new String[] { "Deutsch", "English" }).build();

        assertTrue(checksum(fileString).equalsIgnoreCase(checksum(request.getESQuery().toString())));

    }
    
    @Test
    public void testESQuery_Prefrences_2() throws NoSuchAlgorithmException, IOException {

        String fileString = new String(
                Files.readAllBytes(Paths.get(
                        getClass().getClassLoader().getResource("elasticsearch/query/preferences.json").getFile())),
                StandardCharsets.UTF_8);

        MovieFilter request = MovieFilter.builder()
                .withCast(new String[]{"bar1", "bar2", "bar3", "bar4"}).withCrew("director", new String[]{"foo1", "foo2", "foo3"})
                .withLanguage(new String[] { "Deutsch", "English" }).build();

        assertTrue(checksum(fileString).equalsIgnoreCase(checksum(request.getESQuery().toString())));

    }

    @Test
    public void testESQuery_searchText_Prefrences() throws NoSuchAlgorithmException, IOException {

        String fileString = new String(
                Files.readAllBytes(Paths.get(getClass().getClassLoader()
                        .getResource("elasticsearch/query/searchText_preferences.json").getFile())),
                StandardCharsets.UTF_8);

        MovieFilter request = MovieFilter.builder().withSearchText(Arrays.asList("foo"))
                .withCast(new String[]{"bar1", "bar2", "bar3", "bar4"}).withCrew("director",new String[]{"foo1", "foo2", "foo3"})
                .withLanguage(new String[] { "Deutsch", "English" }).build();

        assertTrue(checksum(fileString).equalsIgnoreCase(checksum(request.getESQuery().toString())));

    }

    @Test
    public void testESQuery_empty() throws NoSuchAlgorithmException, IOException {
        String fileString = new String(
                Files.readAllBytes(Paths.get(getClass().getClassLoader()
                        .getResource("elasticsearch/query/empty.json").getFile())),
                StandardCharsets.UTF_8);
        
        MovieFilter request = MovieFilter.builder().build();
        assertTrue(checksum(fileString).equalsIgnoreCase(checksum(request.getESQuery().toString())));
    }

    private String checksum(String str) throws NoSuchAlgorithmException {

        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(str.getBytes());
        byte[] digest = md.digest();
        String myHash = DatatypeConverter.printHexBinary(digest).toUpperCase();
        return myHash;
    }
}
