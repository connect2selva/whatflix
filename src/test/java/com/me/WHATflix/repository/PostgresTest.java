package com.me.WHATflix.repository;

import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;

import java.time.Duration;

import org.flywaydb.core.Flyway;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.TestPropertySourceUtils;
import org.testcontainers.containers.PostgreSQLContainer;

import com.me.WHATflix.repository.PostgresTest.Initializer;
@RunWith(SpringRunner.class)
@DataJpaTest
@ContextConfiguration(initializers = {Initializer.class})
@AutoConfigureTestDatabase(replace = NONE)
public class PostgresTest {

    @SuppressWarnings("rawtypes")
    public static PostgreSQLContainer postgreSQLContainer = (PostgreSQLContainer) new PostgreSQLContainer(
            "postgres:9.6");

    public static class Initializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
        public Initializer() {
        }
        public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
            postgreSQLContainer.start();
            TestPropertySourceUtils.addInlinedPropertiesToEnvironment(configurableApplicationContext,
                    "spring.datasource.url=" + postgreSQLContainer.getJdbcUrl(),
                    "spring.datasource.username=" + postgreSQLContainer.getUsername(),
                    "spring.datasource.password=" + postgreSQLContainer.getPassword());

            Flyway flyway = new Flyway();
            flyway.setDataSource(postgreSQLContainer.getJdbcUrl(), postgreSQLContainer.getUsername(),
                    postgreSQLContainer.getPassword());
            flyway.migrate();
        }

    }
    
    @Test
    public void testFetchData() {
        System.out.println("Success");
    }
}
