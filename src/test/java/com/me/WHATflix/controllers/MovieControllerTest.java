package com.me.WHATflix.controllers;

import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.me.WHATflix.resources.UserMovie;
import com.me.WHATflix.service.MovieService;

@RunWith(SpringRunner.class)
@WebMvcTest(MovieController.class)
public class MovieControllerTest {

	@Autowired
	MockMvc mockMvc;

	@MockBean
	MovieService movieServiceMock;

	@Test
	public void testFindMoviesByUserId() throws Exception {
		// given
		given(movieServiceMock.findMoviesByUserId(100, Arrays.asList(new String[] { "Tom hanks", "Steven Spielberg" })))
				.willReturn(Arrays.asList(new String[] { "Movie1", "Movie2", "Movie3" }));

		// when & then
		mockMvc.perform(get("/movies/user/100/search?text=Tom hanks,Steven Spielberg")).andExpect(status().isOk())
				.andExpect(jsonPath("$[0]", is("Movie1")));

	}

	@Test
	public void testFindMoviesByUsers() throws Exception {
		// given
	    UserMovie u1 = new UserMovie(100, Arrays.asList(new String[] { "Movie1", "Movie2", "Movie3" }));
	    UserMovie u2 = new UserMovie(101, Arrays.asList(new String[] { "Movie3", "Movie4", "Movie5" }));
		List<UserMovie> list = new ArrayList<>();
		list.add(u1);
		list.add(u2);

		given(movieServiceMock.findMoviesForUsers()).willReturn(list);

		// when & then
		mockMvc.perform(get("/movies/users")).andExpect(status().isOk()).andExpect(jsonPath("$[0].user", is(100)))
				.andExpect(jsonPath("$[0].movies[0]", is("Movie1")))
				.andExpect(jsonPath("$[1].movies[0]", is("Movie3")));
		;

	}

}
