package com.me.WHATflix.controllers;

import static org.junit.Assert.assertEquals;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;

import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.me.WHATflix.config.ESConfig;
import com.me.WHATflix.repository.ElasticSearch;
import com.me.WHATflix.repository.Postgres;
import com.me.WHATflix.resources.UserMovie;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ContextConfiguration(initializers = { Postgres.Initializer.class, ElasticSearch.Initializer.class })
@AutoConfigureTestDatabase(replace = NONE)
public class MovieControllerIT {

    @Autowired
    TestRestTemplate restTemplate;

    @LocalServerPort
    int port;

    @TestConfiguration
    static class TestContextConfiguration {
        @Bean
        public ESConfig esConfig() {
            return ElasticSearch.esConfig();
        }
    }

    Supplier<String> host = () -> String.format("http://localhost:%s", port);

    @Test
    public void testFindMoviesByUserId() {
        ResponseEntity<String[]> res = restTemplate
                .getForEntity(String.format("%s/movies/user/%s/search?text=tom", host.get(), 100), String[].class);

        assertEquals(res.getStatusCode(), HttpStatus.OK);
        assertEquals(res.getBody().length, 4);

    }

    @Test
    public void testFindMoviesForUsers() {
        ResponseEntity<UserMovie[]> res = restTemplate.getForEntity(String.format("%s/movies/users", host.get()), UserMovie[].class);

        assertEquals(res.getStatusCode(), HttpStatus.OK);
        
        List<UserMovie> favMovies = Arrays.asList(res.getBody());

        assertEquals(favMovies.stream().filter(um -> um.getUser() == 100).findFirst().get().getMovies().get(0),
                "Indiana Jones and the Kingdom of the Crystal Skull");

    }
}
