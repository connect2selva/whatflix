### What is this repository for? ###

* Restful services to choose movies for King Shan family
* 1.0.0

### How do I get set up? ###

* [Install Docker](https://www.docker.com/products/docker-engine#/download)
* Checkout the code where you see fit
* Compile & Execute: `deploy/build.sh && docker-compose up`

### Assumptions ###
* Movies would be filtered based olny when all the user preferences matched

### API Documentation ###
http://localhost:8080/swagger-ui.html

### Component Diagram ###
![Component Diagram](https://bitbucket.org/connect2selva/whatflix/raw/5f6984331d33de9de2b6f42c0d83c5e555b4aa39/componentDiagram.png)


### Monitoring ###

* Metrics - http://localhost:8080/metrics
* Health Check - http://localhost:8080/health
* Trace - http://localhost:8080/trace
* Logs - http://localhost:8080/loggers

### Improvements ###
* Authentication & Authorization
* Integrate with Datadog or graphite to monitor real time

### Who do I talk to? ###

* Selvakumar Ponnusamy - selvakumar.ponnusamy@gmail.com
