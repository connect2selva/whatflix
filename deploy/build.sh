#!/bin/bash

mkdir -p ${HOME}/.m2

docker run -it --rm  -v  $PWD:$PWD  -v /var/run/docker.sock:/var/run/docker.sock  -v ${HOME}/.m2:/root/.m2  -w $PWD maven:3.5.2-jdk-8 mvn clean install
docker build --no-cache -t whatflix-svc:latest .
