#!/bin/bash

CONFIG_FILE="/usr/src/whatflix/startup.yml"

KEY=$(echo $ENVIRONMENT | tr '[:upper:]' '[:lower:]')

if [ -n "${KEY}" -a "${KEY}" != "local" ]; then
	# Copy respective environment property file and execute with it
	echo 'Non Local Environment'
else
  JAVA_DEBUG_OPTS="-agentlib:jdwp=transport=dt_socket,server=y,address=8000,suspend=n"
  exec \
    "java" \
    "${JAVA_DEBUG_OPTS}" \
    "-jar" \
    "/usr/src/whatflix/WHATflix.jar" \
    "--spring.config.location=${CONFIG_FILE}"
fi