FROM openjdk:8-jre-alpine

COPY ./target/WHATflix-1.0.0.jar /usr/src/whatflix/WHATflix.jar
COPY ./deploy/startup.yml /usr/src/whatflix/startup.yml


EXPOSE 8080

COPY ./deploy/run.sh /opt/run.sh
RUN ["chmod", "+x", "/opt/run.sh"]

CMD ["sh","/opt/run.sh"]